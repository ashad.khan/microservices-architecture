start: ## Build and start the containers
	docker-compose -f ./AppModule/docker-compose.yml up -d --build
	docker-compose -f ./AuthModule/docker-compose.yml up -d --build
	docker-compose -f ./PokemonModule/docker-compose.yml up -d --build
	docker-compose -f ./UIModule/docker-compose.yml up -d --build

stop: ## Stop containers
	docker-compose -f ./UIModule/docker-compose.yml down
	docker-compose -f ./PokemonModule/docker-compose.yml down
	docker-compose -f ./AuthModule/docker-compose.yml down
	docker-compose -f ./AppModule/docker-compose.yml down

install: ## Install dependencies
	docker exec -u dev -it app_module_web composer install
	docker exec -u dev -it app_module_web bin/console cache:clear --no-warmup

	docker exec -u dev -it auth_module_web composer install
	docker exec -u dev -it auth_module_web bin/console lexik:jwt:generate-keypair --skip-if-exists
	docker exec -u dev -it auth_module_web bin/console doctrine:migrations:migrate -n --allow-no-migration
	docker exec -u dev -it auth_module_web bin/console cache:clear --no-warmup

	docker exec -u dev -it pokemon_module_web composer install
	docker exec -u dev -it pokemon_module_web bin/console doctrine:mongodb:schema:update
	docker exec -u dev -it pokemon_module_web bin/console cache:clear --no-warmup

app_web: ## Connect to app container
	docker exec -u dev -it app_module_web bash

auth_web: ## Connect to auth container
	docker exec -u dev -it auth_module_web bash

auth_db: ## Connect to auth db container
	docker exec -it auth_module_mysql bash

pokemon_web: ## Connect to pokemon container
	docker exec -u dev -it pokemon_module_web bash

pokemon_db: ## Connect to pokemon db container
	docker exec -it pokemon_module_mysql bash

ui_web: ## Connect to ui container
	docker exec -it ui_module_web bash

init: ## Init project
	make start
	make install
