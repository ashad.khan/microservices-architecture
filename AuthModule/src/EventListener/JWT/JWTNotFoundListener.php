<?php

namespace App\EventListener\JWT;

use App\Exception\AuthApiException;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTNotFoundEvent;

class JWTNotFoundListener
{
    /**
     * @param JWTNotFoundEvent $event
     */
    public function onJWTNotFound(JWTNotFoundEvent $event)
    {
        throw new AuthApiException($event->getException()->getMessage());
    }
}