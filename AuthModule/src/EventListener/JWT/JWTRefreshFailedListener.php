<?php

namespace App\EventListener\JWT;

use App\Exception\AuthApiException;
use App\Exception\JWTRefreshFailedException;
use Gesdinet\JWTRefreshTokenBundle\Event\RefreshAuthenticationFailureEvent;

class JWTRefreshFailedListener
{
    /**
     * @param RefreshAuthenticationFailureEvent $event
     */
    public function onJWTRefreshFailed(RefreshAuthenticationFailureEvent $event)
    {
        throw new JWTRefreshFailedException($event->getException()->getMessage());
    }
}