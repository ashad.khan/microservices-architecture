<?php

namespace App\EventListener\JWT;

use App\Exception\AuthApiException;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTInvalidEvent;

class JWTInvalidListener
{
    /**
     * @param JWTInvalidEvent $event
     */
    public function onJWTInvalid(JWTInvalidEvent $event)
    {
        throw new AuthApiException($event->getException()->getMessage());
    }
}