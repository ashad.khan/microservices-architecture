<?php

namespace App\EventListener;

use App\Exception\AuthApiException;
use App\Exception\InvalidDataException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use function PHPUnit\Framework\isEmpty;

class ExceptionListener
{
    private $logger;
    private $translator;

    public function __construct(LoggerInterface $logger, TranslatorInterface $translator)
    {
        $this->logger = $logger;
        $this->translator = $translator;
    }

    private function logMessage(string $message)
    {
        $this->logger->error(
            sprintf(
                "[API AUTH] %s",
                $message
            )
        );
    }

    public function onKernelException(ExceptionEvent $event)
    {
        // You get the exception object from the received event
        $exception = $event->getThrowable();

        // Customize your response object to display the exception details
        $response = new JsonResponse();

        $data = [];

        // HttpExceptionInterface is a special type of exception that
        // holds status code and header details
        if ($exception instanceof InvalidDataException) {
            $data['code'] = $exception->getStringCode();
            $data['errors'] = $exception->getErrors();
        } elseif ($exception instanceof AuthApiException) {
            $data['code'] = $exception->getStringCode();
            $response->setStatusCode($exception->getStatusCode());
            $this->logMessage($exception->getMessage());
        } elseif ($exception instanceof HttpExceptionInterface &&
            $exception->getStatusCode() < Response::HTTP_INTERNAL_SERVER_ERROR
        ) {
            $data['code'] = 'AUTH_HTTP_ERROR';
            $data['message'] = $this->translator->trans($exception->getMessage());
            $response->setStatusCode($exception->getStatusCode());
        } else {
            $data['code'] = AuthApiException::CODE;
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
            $this->logMessage($exception->getMessage());
        }

        $response->setData($data);

        // sends the modified response object to the event
        $event->setResponse($response);
    }
}