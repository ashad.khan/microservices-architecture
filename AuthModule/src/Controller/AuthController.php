<?php

namespace App\Controller;

use App\Dto\RegisterUserRequestDto;
use App\Service\AuthInterface;
use JMS\Serializer\SerializerBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AuthController extends AbstractController
{
    private $authService;
    private $serializer;

    public function __construct(AuthInterface $authService)
    {
        $this->authService = $authService;
        $this->serializer = SerializerBuilder::create()->build();
    }


    /**
     * @Route("/register", name="auth_register", methods="POST")
     */
    public function register(Request $request): JsonResponse
    {
        $jsonContent = $request->getContent();

        $registerUserRequestDto = $this->serializer->deserialize($jsonContent, RegisterUserRequestDto::class, 'json');
        $registerUserResponseDto = $this->authService->register($registerUserRequestDto);

        return $this->json($this->serializer->toArray($registerUserResponseDto));
    }
}
