<?php

namespace App\Service;

use App\Dto\RegisterUserRequestDto;
use App\Dto\RegisterUserResponseDto;
use App\Entity\User;
use App\Validator\DataValidatorInterface;
use App\Validator\Model\Validation;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AuthService extends AuthAppService implements AuthInterface
{
    private $encoder;

    public function __construct(
        EntityManagerInterface $entityManager,
        DataValidatorInterface $validator,
        UserPasswordHasherInterface $encoder
    ) {
        parent::__construct($entityManager, $validator);
        $this->encoder = $encoder;
    }

    public function register(RegisterUserRequestDto $requestDto): RegisterUserResponseDto
    {
        $user = $requestDto->toUser();
        $requestValidation = new Validation($requestDto);
        $userValidation = new Validation($user);

        $this->validator->validate([$userValidation, $requestValidation]);

        $user->setPassword($this->encoder->hashPassword($user, $user->getPassword()));
        $this->save($user);

        $responseDto = new RegisterUserResponseDto();
        $responseDto->fromUser($user);

        return $responseDto;
    }
}