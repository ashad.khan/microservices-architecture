<?php

namespace App\Service;

use App\Validator\DataValidatorInterface;
use Doctrine\ORM\EntityManagerInterface;

abstract class AuthAppService
{
    protected $entityManager;
    protected $validator;

    public function __construct(EntityManagerInterface $entityManager, DataValidatorInterface $validator)
    {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
    }

    public function save($entity, $flush = true)
    {
        $this->entityManager->persist($entity);

        if ($flush) {
            $this->entityManager->flush();
        }
    }
}