<?php

namespace App\Service;

use App\Dto\RegisterUserRequestDto;
use App\Dto\RegisterUserResponseDto;

interface AuthInterface
{
    public function register(RegisterUserRequestDto $requestDto): RegisterUserResponseDto;
}