<?php

namespace App\Dto;

use App\Entity\User;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

class RegisterUserRequestDto
{
    /**
     * @Type("string")
     * @Assert\NotBlank(message = "user.email.not_blank")
     * @Assert\Email(message = "user.email.email")
     */
    private $email;

    /**
     * @Type("string")
     * @Assert\NotBlank(message = "user.password.not_blank")
     * @Assert\Regex(
     *     pattern = "/^(?=.*[!@#$%^&*-])(?=.*[0-9])(?=.*[A-Z]).*$/",
     *     message = "user.password.regex"
     * )
     * @Assert\Length(
     *     min=6,
     *     max=20,
     *     minMessage = "user.password.length.min_message",
     *     maxMessage = "user.password.length.max_message"
     * )
     */
    private $password;

    public function toUser(): User
    {
        $user = new User();
        $user->setEmail($this->email);
        $user->setPassword($this->password);

        return $user;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return RegisterUserRequestDto
     */
    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     * @return RegisterUserRequestDto
     */
    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }
}