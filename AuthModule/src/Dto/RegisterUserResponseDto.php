<?php

namespace App\Dto;

use App\Entity\User;

class RegisterUserResponseDto
{
    private $email;

    public function fromUser(User $user): self
    {
        $this->email = $user->getEmail();

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return RegisterUserResponseDto
     */
    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }
}