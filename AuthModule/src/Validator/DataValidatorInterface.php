<?php

namespace App\Validator;

use App\Validator\Model\Validation;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\GroupSequence;
use Symfony\Component\Validator\ConstraintViolationListInterface;

interface DataValidatorInterface
{
    /**
     * @param Validation|Validation[] $validations
     */
    public function validate($validations);
}