<?php

namespace App\Validator\Model;

use Symfony\Component\Validator\Constraint;

class Validation
{
    private $value;
    private $constraints;
    private $groups;

    public function __construct($value, $constraints = null, $groups = null)
    {
        $this->value = $value;
        $this->constraints = $constraints;
        $this->groups = $groups;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return mixed|null
     */
    public function getConstraints()
    {
        return $this->constraints;
    }

    /**
     * @return mixed|null
     */
    public function getGroups()
    {
        return $this->groups;
    }


}