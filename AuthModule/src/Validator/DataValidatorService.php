<?php

namespace App\Validator;

use App\Exception\InvalidDataException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class DataValidatorService implements DataValidatorInterface
{
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function validate($validations)
    {
        $mergedViolations = null;

        if (is_array($validations)) {
            foreach ($validations as $validation) {
                $violations = $this->validator->validate(
                    $validation->getValue(),
                    $validation->getConstraints(),
                    $validation->getGroups()
                );

                if (is_null($mergedViolations)) {
                    $mergedViolations = $violations;
                } else {
                    $mergedViolations->addAll($violations);
                }
            }
        } else {
            $mergedViolations = $this->validator->validate(
                $validations->getValue(),
                $validations->getConstraints(),
                $validations->getGroups()
            );
        }

        if (count($mergedViolations) > 0) {
            throw new InvalidDataException($mergedViolations);
        }
    }
}