<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

class JWTExpiredException extends AuthApiException
{
    public function __construct(?string $message = '', \Throwable $previous = null, array $headers = [], ?int $code = 0)
    {
        parent::__construct($message, Response::HTTP_UNAUTHORIZED, $previous, $headers, $code);
    }

    public function getStringCode(): string
    {
        return "AUTH_TOKEN_EXPIRED";
    }
}