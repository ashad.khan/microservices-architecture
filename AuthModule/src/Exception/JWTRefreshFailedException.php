<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

class JWTRefreshFailedException extends AuthApiException
{
    public function __construct(?string $message = '', \Throwable $previous = null, array $headers = [], ?int $code = 0)
    {
        parent::__construct($message, Response::HTTP_UNAUTHORIZED, $previous, $headers, $code);
    }

    public function getStringCode(): string
    {
        return "AUTH_REFRESH_TOKEN_FAILED";
    }
}