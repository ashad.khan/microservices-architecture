<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AuthApiException extends HttpException
{
    const CODE = 'AUTH_INTERNAL_ERROR';

    public function __construct(
        ?string $message = '',
        int $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR,
        \Throwable $previous = null,
        array $headers = [],
        ?int $code = 0
    ) {
        parent::__construct($statusCode, $message, $previous, $headers, $code);
    }

    public function getStringCode(): string
    {
        return self::CODE;
    }
}