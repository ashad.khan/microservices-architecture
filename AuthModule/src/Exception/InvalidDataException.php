<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class InvalidDataException extends AuthApiException
{
    private $violations;

    public function __construct(
        ConstraintViolationListInterface $violations
    ) {
        parent::__construct(null,Response::HTTP_BAD_REQUEST);
        $this->violations = $violations;
    }

    public function getStringCode(): string
    {
        return 'AUTH_INVALID_DATA';
    }

    /**
     * @throws \ReflectionException
     */
    public function getErrors(): array
    {
        $errors = [];
        foreach ($this->violations as $violation) {
            $constraint = $violation->getConstraint();
            $reflect = new \ReflectionClass($constraint);
            $constraintName = strtolower($reflect->getShortName());

            $errors[$violation->getPropertyPath()][$constraintName] = $violation->getMessage();
        }

        return $errors;
    }
}