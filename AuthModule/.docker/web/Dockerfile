FROM php:7.4-apache

ARG USER=dev

# GLOBAL INSTALLATION
RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
        libzip-dev \
        zlib1g-dev \
        libicu-dev \
        sudo \
        acl \
        unzip \
        zip \
        git \
        nano \
        curl \
        gnupg \
        g++ \
        libcurl4-openssl-dev \
        pkg-config \
        libssl-dev \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-install zip pdo pdo_mysql opcache \
    && docker-php-ext-configure intl \
    && docker-php-ext-install intl \
    && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# APACHE CONFIGURATION
COPY ./apache/app.conf /etc/apache2/sites-available/000-default.conf
COPY ./php/php.ini /usr/local/etc/php/conf.d/app.ini

# COMPOSER INSTALLATION
RUN curl --silent --show-error https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# SYMFONY CLI INSTALLATION
RUN echo 'deb [trusted=yes] https://repo.symfony.com/apt/ /' | sudo tee /etc/apt/sources.list.d/symfony-cli.list
RUN apt update
RUN apt install symfony-cli

# LOCAL TIME SETTING
RUN rm /etc/localtime
RUN ln -s /usr/share/zoneinfo/Europe/Paris /etc/localtime
RUN "date"

# APACHE EXTENSIONS
RUN a2enmod rewrite
RUN a2enmod headers

# SYSTEM USER
RUN groupadd ${USER} -g 1000 \
    && useradd ${USER} -g ${USER} -d /home/${USER} -m -ms /bin/bash