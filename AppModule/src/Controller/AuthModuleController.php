<?php

namespace App\Controller;

use App\Service\AuthModuleInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AuthModuleController extends AbstractController
{
    private $authModuleService;

    public function __construct(AuthModuleInterface $authModuleService)
    {
        $this->authModuleService = $authModuleService;
    }

    /**
     * @Route("/auth/login", name="auth_login")
     */
    public function login(Request $request): JsonResponse
    {
        $data = $this->authModuleService->login(json_decode($request->getContent(), true));

        return $this->json($data);
    }

    /**
     * @Route("/auth/register", name="auth_register")
     */
    public function register(Request $request): JsonResponse
    {
        $data = $this->authModuleService->register(json_decode($request->getContent(), true));

        return $this->json($data);
    }

    /**
     * @Route("/auth/token/refresh", name="auth_refresh_token")
     */
    public function refreshToken(Request $request): JsonResponse
    {
        $data = $this->authModuleService->refreshToken(json_decode($request->getContent(), true));

        return $this->json($data);
    }

    /**
     * @Route("/auth/token/invalidate", name="auth_invalidate_token")
     */
    public function invalidateToken(Request $request): JsonResponse
    {
        $data = $this->authModuleService->invalidateToken(json_decode($request->getContent(), true));

        return $this->json($data);
    }
}
