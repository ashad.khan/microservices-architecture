<?php

namespace App\Service;

use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class AuthModuleService extends AppModuleService implements AuthModuleInterface
{
    private $authModuleUrl;

    public function __construct(HttpClientInterface $client, string $authModuleUrl)
    {
        parent::__construct($client);
        $this->authModuleUrl = $authModuleUrl;
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function login($data): array
    {
        $response = $this->client->request(
            'POST',
            $this->authModuleUrl . '/login_check',
            [
                'json' => $data
            ]
        );

        return $response->toArray();
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function register($data): array
    {
        $response = $this->client->request(
            'POST',
            $this->authModuleUrl . '/register',
            [
                'json' => $data
            ]
        );

        return $response->toArray();
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function refreshToken($data): array
    {
        $response = $this->client->request(
            'POST',
            $this->authModuleUrl . '/token/refresh',
            [
                'json' => $data
            ]
        );

        return $response->toArray();
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function invalidateToken($data): array
    {
        $response = $this->client->request(
            'POST',
            $this->authModuleUrl . '/token/invalidate',
            [
                'json' => $data
            ]
        );

        return $response->toArray();
    }
}