<?php

namespace App\Service;

interface AuthModuleInterface
{
    public function login($data): array;
    public function register($data): array;
    public function refreshToken($data): array;
    public function invalidateToken($data): array;
}