<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AppApiException extends HttpException
{
    const CODE = 'INTERNAL_ERROR';

    public function getStringCode(): string
    {
        return self::CODE;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return Response::HTTP_INTERNAL_SERVER_ERROR;
    }
}